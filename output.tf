output "classic_lb_dns" {
  value = aws_elb.classic.dns_name
}

output "classic_lb_id" {
  value = aws_elb.classic.id
}

output "classic_lb_arn" {
  value = aws_elb.classic.arn
}

output "classic_lb_name" {
  value = aws_elb.classic.name
}

output "classic_lb_instances" {
  value = aws_elb.classic.instances
}

output "classic_lb_zone_id" {
  value = aws_elb.classic.zone_id
}


