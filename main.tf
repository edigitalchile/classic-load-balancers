resource "aws_elb" "classic" {
  name            = var.name
  subnets         = var.subnets_ids
  security_groups = var.security_groups_ids

  dynamic "listener" {

    for_each = var.listeners

    content {
      instance_port      = listener.value.instance_port
      instance_protocol  = listener.value.instance_protocol
      lb_port            = listener.value.lb_port
      lb_protocol        = listener.value.lb_protocol
      ssl_certificate_id = lookup(listener.value, "ssl_certificate_id", null)
    }
  }

  health_check {
    healthy_threshold   = var.health_check.healthy_threshold
    unhealthy_threshold = var.health_check.unhealthy_threshold
    timeout             = var.health_check.timeout
    target              = var.health_check.target
    interval            = var.health_check.interval
  }

  instances                   = var.target_instances_ids
  cross_zone_load_balancing   = var.cross_zone_load_balancing
  idle_timeout                = var.idle_timeout
  connection_draining         = var.connection_draining
  connection_draining_timeout = var.connection_draining_timeout
  tags                        = var.tags

}


resource "aws_load_balancer_policy" "classic_ssl_policy" {
  count              = length(var.policy_attributes_ssl) > 0 ? 1 : 0
  load_balancer_name = aws_elb.classic.name
  policy_name        = var.policy_name
  policy_type_name   = "SSLNegotiationPolicyType"

  dynamic "policy_attribute" {
    for_each = var.policy_attributes_ssl

    content {
      name  = policy_attribute.value.name
      value = policy_attribute.value.value
    }
  }


}

resource "aws_load_balancer_listener_policy" "classic_listener_policies_443" {
  count              = length(var.policy_attributes_ssl) > 0 ? 1 : 0
  load_balancer_name = aws_elb.classic.name
  load_balancer_port = 443

  policy_names = [
    aws_load_balancer_policy.classic_ssl_policy[0].policy_name,
  ]

}