# Modulo de terraform para crear balanceadores clasicos

### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| name  | Nombre del balanceador de carga clasico  | SI  |  N/A |
| subnets_ids  |  Lista de ids de subredes donde estara el balanceador | SI  | N/A  |
| security_groups_ids | Lista de ids de los security groups que se le aplicaran al balanceador  | SI  | N/A  |
| listeners  | Lista de listeners para el balanceador  | SI  | N/A  |
| health_check  | Configuracion del health check  | SI  | N/A  |
| target_instances_ids  | Lista de ids de las intancias donde apuntara el balanceador  | SI  | N/A  |
| cross_zone_load_balancing  | Habilitar o desahbilitar cross zone load balancing  | NO  | true  |
| connection_draining  | Habilitar o desahbilitar connection drainning  | NO  | true  |
| idle_timeout  | Tiempo maximo de espera de respuesta del servidor en segundos  | NO  | 60  |
| connection_draining_timeout  | Tiempo maximo de espera para volver a chequear el estado del servidor en segundos  | NO  | 60  |
| policy_attributes_ssl  | Configuracion ssl para listener https  | NO  | []  |
| policy_name  | Nombre del conjunto de policies para listener https  | NO  | null  |
| tags  | Mapa de tags para asignar al load balancer | NO  | null  |


### Outputs

| Nombre  | Descripción  | 
|---|---|
| classic_lb_dns  | Dns generado para el balanceador  |
| classic_lb_id  |  Id del balanceador creado |
| classic_lb_arn  | Arn del balanceador creado  |
| classic_lb_name  | Nombre del balanceador creado  |
| classic_lb_instances  | Instancias asociadas al balanceador creado  |
| classic_lb_zone_id  | Id de la zona doste esta hosteado el balanceador creado(Para ser usado con route53)  |