#REQUIRED PARAMETERS

variable name {
  type        = string
  description = "Nombre del balanceador de carga clasico"
}

variable subnets_ids {
  type        = list(string)
  description = "Subredes donde estara el balanceador"
}

variable security_groups_ids {
  type        = list(string)
  description = "Ids de los security groups que se le aplicaran al balanceador"
}

variable listeners {
  type        = list(map(any))
  description = "Lista de listeners para el balanceador"
}

variable health_check {
  type        = map(any)
  description = "Configuracion del health check"
}

variable target_instances_ids {
  type        = list(string)
  description = "Lista de ids de las intancias donde apuntara el balanceador"
}

# OPTIONAL PARAMETERS

variable cross_zone_load_balancing {
  type        = bool
  default     = true
  description = "Habilitar o desahbilitar cross zone load balancing"
}

variable connection_draining {
  type        = bool
  default     = true
  description = "Habilitar o desahbilitar connection drainning"
}

variable idle_timeout {
  type        = number
  default     = 60
  description = "Tiempo maximo de espera de respuesta del servidor"
}

variable connection_draining_timeout {
  type        = number
  default     = 60
  description = "Tiempo maximo de espera para volver a chequear el estado del servidor"
}

variable policy_attributes_ssl {
  type        = list(map(string))
  default     = []
  description = "Configuracion ssl para listener https"
}

variable policy_name {
  type        = string
  default     = null
  description = "Nombre del conunto de policies para listener https"
}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar al load balancer"
  default     = null
}









